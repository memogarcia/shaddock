============
Installation
============

At the command line::

    $ pip install shaddock

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv shaddock
    $ pip install shaddock
